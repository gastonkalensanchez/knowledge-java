
package ejerciocios.java;

import java.util.Scanner;


public class EjerciociosJava {

    static Scanner Entrada = new Scanner(System.in);
    public static void main(String[] args)
    {
        //Ejercicio1();
        //Ejercicio2();
        //Ejercicio3();
    }
    
    public static void Ejercicio1()
    {
        /*
        Hacer un programa que calcule e
        imprima la suma de 3 calificaciones.
        */
      
        
        float Numero1, Numero2,Numero3,Suma;
        System.out.println("Ingrese 3 numeros: ");
        Numero1 = Entrada.nextFloat();
        Numero2 = Entrada.nextFloat();
        Numero3 = Entrada.nextFloat();
        Suma = Numero1 + Numero2 + Numero3;
        System.out.println("La suma de los 3 es: " + Suma);                
    }
    public static void Ejercicio2()
    {
        /*  
        Hacer un programa que calcule e imprima
        el salario semanal de un empleado a partir
        de sus horas semanales trabajadas y de su 
        salario por hora
        */
        float SalarioSemanal,HorasTrabajadas,SalarioPorHora;
        
        System.out.println("Dijite sus salario semanal");
        SalarioSemanal = Entrada.nextFloat();
        System.out.println("Dijite las horas trabajadas: ");
        HorasTrabajadas = Entrada.nextFloat();
        SalarioPorHora = SalarioSemanal * HorasTrabajadas;
        System.out.println("Su salario por sus horas es: " + SalarioPorHora);
       
        
        
        
    }
    public static void Ejercicio3()
    {
        /*
        Guillermo tiene N dolares. Luis tiene la mitad
        de lo que tiene Guillermo. Juan tiene la mitad 
        de lo que tiene Luis y Guillermo juntos. hacer un
        programa que calcule e imprima la cantidad de dinero
        que tienen entre los 3
        */
        int Guillermo, Luis,Juan;
        System.out.println("Cuanto dolares tiene Guillermo: ");
        Guillermo = Entrada.nextInt();
        Luis = Guillermo / 2;
        Juan = (Guillermo + Luis) / 2;
        System.out.println("Los Dolares de Guillermo son: " + Guillermo);
        System.out.println("Los Dolares de Luis son: " + Luis);
        System.out.println("Los Dolares de Juan son: " + Juan);
    }
    public static void Ejercicio4()
    {
        /*
        Una compañia de venta de autos usados, paga
        a su personal de ventas un salario de 1000 mensuales,
        mas una comision por 150 por cada auto vendido, mas el
        5% del valor de la venta por auto. Cada mes el capturista
        de la empresa ingresa en la computadora los datos pertinenetes.
        hacer un programa que calcule e imprima el salario mensual de 
        un vendedor.
        */
        float salario = 1000, Comision = 150,Autos,FinalSaldo;
        float ComisionPorAuto = 0.05f;
        
        System.out.println("Cuantos Autos venidio: ");
        Autos = Entrada.nextInt();
        FinalSaldo = (salario + (Autos * Comision)) * ComisionPorAuto;
        
    }
}
